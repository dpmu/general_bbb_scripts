#! /bin/bash
COUNTER_RESET_FILE="/usr/midnight_counter_resets.txt"

###############################################################################
# This scripts is a executable script scheduled by the crontab. It is used to #
#	restart the D-PMU cleaning the cache file and temporary data. 			  #
###############################################################################

#Debug part : Write the number of reset into number of reset file

#verify if the number of reboots file exists.
if [ -f "$COUNTER_RESET_FILE" ]; then #file exists, increment the value internal

	#read the file and get the restarts counter value
	count=$(cat $COUNTER_RESET_FILE)

	let "count=$count + 1" #increment the value
	echo $count>$COUNTER_RESET_FILE
	
else #Create the file with 1 value

	# create and write initial value into the file
	echo 1 > $COUNTER_RESET_FILE
	
fi

#reboot the D-PMU
reboot now 
