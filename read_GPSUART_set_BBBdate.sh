#! /bin/bash

##############################################################################
# This scripts is used to open the D-PMU GPS uart port to read the NMEA  	 #
# datetime frame, decode it and set the D-PMU date. This date is used to	 #
# calculate the midnigth our and reset the BBB.							     #
# The echos are used to log the script steps into log file.					 #
##############################################################################

#definitions:
C_GPS_SERIAL_PORT_NAME="/dev/ttyS4"
C_INTEREST_FRAME="GPRMC"

echo "############# Begin: read_GPSUART_set_BBBdate script #############"
echo ""
echo "3 seconds to wait a idle state on Linux"
sleep 3

#Enable the pin uart P9_11 on Beaglebone
echo ""
echo "configuring GPS pin"
config-pin -a P9_11 uart 	#Uart pin to ARM

#example og GPRMC frame
#GPRMC,hhmmss.ss,A,llll.llll,S,yyyy.yy,W,s.s,x.x,ddmmyy,,,A*6C
#https://www.marvelmind.com/downloads/marvelmind_NMEA0183_PPS_v2016_09_01.pdf

#Open serial, read data and get the GPRMC frame
echo ""
echo "Reading data from the $C_GPS_SERIAL_PORT_NAME device (serial port)"
gpsData=$(timeout 2 grep $C_INTEREST_FRAME -m 1 < $C_GPS_SERIAL_PORT_NAME) 2>/dev/null 
	retVal=$? 
	if [ $retVal -ne 0 ]; then 
		echo "Uart did not receive GPS data in the last 2 seconds.."
		gpsData="null"
		echo "gpsData is null"
		echo "Error - Problem reading datetime from the uart device."
	fi

#debug part 
echo ""
echo "GPS complete frame:"
echo "$gpsData"

#Decoding date and time from GPS frame
echo ""
echo "decoding frame"
read date <<< $(echo $gpsData | awk -F"," '{print $10}')
echo "date field collected from the frame"
echo "$date"
read time <<< $(echo $gpsData | awk -F"," '{print $2}')
echo "time field collected from the frame"
echo "$time"

#Get the date-time values
time_hour=${time:0:2}
time_min=${time:2:2}
#time_sec=${gpsData:11:2} 
date_day=${date:0:2}
date_month=${date:2:2}
date_year=${date:4:2}
date_year=$[$date_year+2000]

#debug part
echo ""
echo "Docoded information:"
echo "Date is:"
echo "$date_day / $date_month / $date_year"
echo "Time is:"
echo "$time_hour : $time_min"

echo ""
#Set date on beaglebone
#command: date --set="20121204 04:30"
date --set="$date_year$date_month$date_day $time_hour:$time_min"

echo "############# End: read_GPSUART_set_BBBdate script ############"
