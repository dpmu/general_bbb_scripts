#!/bin/bash

#****************************************************************************#
# 				Script part for PRU0 - CHECK PWM Start                       #
#****************************************************************************#
#To be implemented
#temporaly transform the Convrequest pin from PRU0 to generic GPIO with pull-up
config-pin -a P9_28 gpio_pu
echo 'in' > /sys/class/gpio/gpio113/direction	#same as P9_28
#temporaly transform the P8_29 pin to reboot abort signal. The user just need to
#maintein it pressed. 
config-pin -a P8_29 gpio_pd
echo 'in' > /sys/class/gpio/gpio87/direction	#same as P8_29 = S4 button
if [ $(cat /sys/class/gpio/gpio87/value) -eq 0 ]; then #Just verify(AND REBOOT) if button S4 is released

	#reset for control variables.
	P9_28_get_low=0 
	P9_28_get_high=0
	pwm_working_fine=0
	for i in {1..600} #600
	do
		value_from_convreq_pin=$(cat /sys/class/gpio/gpio113/value)
		if [ $value_from_convreq_pin -eq 1 ]; then
			P9_28_get_high=1	#get a high level over pin
		else
			P9_28_get_low=1		#get a low level over pin
		fi
		if [ $P9_28_get_high -eq $P9_28_get_low ] ; then
			pwm_working_fine=1	#Both signal leves are obtained
			break
		fi
	done

	if [ $pwm_working_fine -eq 0 ]; then

		reboot now
		echo 'ERROR: there is no PWM running status. Rebooting'
	fi
fi

config-pin -a P9_28 pruin 	#Returning the pin mode to PRU0
config-pin -a P8_29 pruin	#Returning the pin mode to PRU1

