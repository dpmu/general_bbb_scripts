#! /bin/bash

##############################################################################
# This scripts is used to test and verify the expected behavior of the D-PMU #
# GPS. It prints the test result after the test execution.                   #  
##############################################################################
#definitions:
C_GPS_SERIAL_PORT_NAME="/dev/ttyS4"
C_INTEREST_FRAME="GPRMC"
C_TIMEOUT="5"

#Starting prints
echo "Starting the GPS status scripts check. It checks:"
echo "  * GPS UART $GPRMC frame (datetime used frame);"
echo "  * GPS 1PPS input signal."

#Enable the pin uart P9_11 on Beaglebone
config-pin -a P9_11 uart 	#Uart pin to ARM
config-pin -a P9_17 gpio_pd #Uart pin to GPIO PD
echo 'in' > /sys/class/gpio/gpio5/direction	#same as P9_17

############################### Check GPS frame read ##########################
echo ""
echo "Reading GPS DateTime frame"
#example og GPRMC frame
#GPRMC,hhmmss.ss,A,llll.llll,S,yyyy.yy,W,s.s,x.x,ddmmyy,,,A*6C
#https://www.marvelmind.com/downloads/marvelmind_NMEA0183_PPS_v2016_09_01.pdf


#Open serial, read data and get the GPRMC frame
gpsData=$(timeout 2 grep $C_INTEREST_FRAME -m 1 < $C_GPS_SERIAL_PORT_NAME) 2>/dev/null 
retVal=$? 
if [ $retVal -ne 0 ]; then 
	echo "GPS UART ERROR. The GPS didn't send the datetime frame."	
else
	echo "GPS UART OK. The DateTime header frame was received sucessfull."
	echo "date(UTC) is ${gpsData:53:2}/${gpsData:55:2}/${gpsData:57:2}"
	echo "time(UTC) is ${gpsData:7:2}:${gpsData:9:2}:${gpsData:11:2}"
fi
	
############################### Check 1PPS pin P9_17 ##########################
#reset OF control variables.
P9_17_get_low=0 
P9_17_get_high=0
pwm_working_fine=0
for i in {1..1600} 
do	
	value_from_convreq_pin=$(cat /sys/class/gpio/gpio5/value)
	if [ $value_from_convreq_pin -eq 1 ]; then
		P9_17_get_high=1	#get a high level over pin
	else
		P9_17_get_low=1		#get a low level over pin
	fi
	if [ $P9_17_get_high -eq $P9_17_get_low ] ; then
		pwm_working_fine=1	#Both signal levels are read. Expected operation
		break
	fi
done
#verify if there a pulse was detected.
if [ $pwm_working_fine -eq 0 ]; then
	echo "GPS 1PPS ERROR. The 1PPS has no pulses. Check the antenna and connections"
else
	echo "GPS 1PPS OK. The 1PPS pin has pulses."
fi

#Returning the P9_17 D-PMU default behavior
config-pin -a P9_17 pwm #Uart pin to D-PMU pwm port 

	
	
	


