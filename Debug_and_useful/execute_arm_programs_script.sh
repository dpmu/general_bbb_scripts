#! /bin/bash



##############################################################################
#
# Main programs in ARM execution
##############################################################################


echo "*******************************************************"
echo "This scripts removes the temporary files (carefull with"
echo "names) and executes teh C_37118_protocol, pmu_beaglebone,"
echo "and Samples handler."
echo " "
echo "Want to send data to Remote Server? [Y/N] or [y/n]"
read varname_flag


echo "removing trash files"
	sudo rm /tmp/Beaglebone_processed_outputs0.txt
	sudo rm /tmp/Beaglebone_processed_outputs1.txt
	sudo rm /tmp/PRU2samplerFIFO/*
	sudo rm /tmp/pmu2C37_118/*
	sudo rm /tmp/handler2pmu/*
	
	

echo "Killing ghost PMU process" 
#Find the Process ID for syncapp running instance
	ps -ef | grep c_37118_protoco | grep -v grep | awk '{print $2}' | xargs kill
	ps -ef | grep pmu_beaglebone | grep -v grep | awk '{print $2}' | xargs kill
	ps -ef | grep Samples_handler | grep -v grep | awk '{print $2}' | xargs kill

	
 
	
echo "Executing the scripts:"
echo "****************************************c37118 program*******************************************"
    nohup sudo nice -n -2 /usr/c37118/c_37118_protocol $varname_flag >/dev/null 2>&1 & 
	sleep 1 #wait for 1 second
echo "****************************************pmu_beaglebone*******************************************"
    nohup sudo nice -n 5 /usr/synchrophasor/pmu_beaglebone >/dev/null 2>&1 &
	sleep 1 #wait for 1 second
echo "****************************************Samples_Handler******************************************"
    nohup sudo nice -n -6 /usr/sampleshandler/Samples_handler >/dev/null 2>&1 &
	sleep 1 #wait for 1 second
echo "*************************************************************************************************"


echo "Finished. Now you can run PRU1 and PRU0 deploys programs."

#nohup sudo ./c_37118_protocol >/dev/null & 