#! /bin/bash

##############################################################################
# This scripts is used just verify the run mode jumper configuration on D-PMU#
##############################################################################
#vdd pin
config-pin -a P8_3 gpio_pu #Allow vdd to the input evaluation pin.
echo 'out' > /sys/class/gpio/gpio38/direction
echo '1' > /sys/class/gpio/gpio38/value	
#gnd pin
config-pin -a P8_7 gpio_pd #Allow gnd to the input evaluation pin.
echo 'out' > /sys/class/gpio/gpio66/direction
echo '0' > /sys/class/gpio/gpio66/value	
#evaluation pin
config-pin -a P8_5 gpio_pd
echo 'in' > /sys/class/gpio/gpio34/direction

############################### Check operation mode ##########################
P9_17_get_high=0
pwm_working_fine=0
for i in {1..30} 
do	
	runmode_value=$(cat /sys/class/gpio/gpio34/value)
	if [ $runmode_value -eq 1 ]; then
		P9_17_get_high=1	#get a high level over pin
		echo "AUTO RUN MODE"
	else
		P9_17_get_low=1		#get a low level over pin
		echo "Wait RUN MODE"
	fi
	sleep 1
	
done

#Returning the P9_17 D-PMU default behavior
config-pin -a P9_17 pwm #Uart pin to D-PMU pwm port 

	
	
	


