#! /bin/bash



##############################################################################
#
# Look for process and output folders to delete then
##############################################################################


echo "*******************************************************"
echo "This scripts removes the temporary files (carefull with"
echo "names)"
echo " "

#
echo "removing trash files"
	sudo rm /tmp/Beaglebone_processed_outputs0.txt
	sudo rm /tmp/Beaglebone_processed_outputs1.txt
	sudo rm /tmp/PRU2samplerFIFO/*
	sudo rm /tmp/pmu2C37_118/*
	sudo rm /tmp/handler2pmu/*
	
	
#Find the Process ID for syncapp running instance
echo "Killing ghost PMU process" 
	echo "kill c_37118_protocol"
	ps -ef | grep c_37118_protocol | grep -v grep | awk '{print $2}' | xargs kill
	echo "kill pmu beaglebone"
	ps -ef | grep pmu_beaglebone | grep -v grep | awk '{print $2}' | xargs kill
	echo "kill Samples_handler"
	ps -ef | grep Samples_handler | grep -v grep | awk '{print $2}' | xargs kill
	##PRUS process
	echo "kill loaderPRU1"
	ps -ef | grep loaderPRU1 | grep -v grep | awk '{print $2}' | xargs kill

#Find the screen session ID to kill
echo "Killing old screen sessions"
	killall screen
	pkill screen
