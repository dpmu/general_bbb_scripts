#! /bin/bash
set -x

##############################################################################
# This scripts is used to turn the D-PMU a plug and play device. It executes #
# the GPIO configuration, deploys PRUSS, execute the Screens for 3 C programs#
# After PRU0 deploy, this scripts checks if the conversions samples are runn-#
# ing. If yes, it continues the scripts, else, it restart beaglebone. 		 #
##############################################################################
echo "################### Begin: plug_and_play script ########################"
echo ""

echo "wait 40 seconds to wait the pwm module initialization"
sleep 40				#Time to wait all linux processes starts
 
#defines and parameters
PRU0_FOLDER=/usr/PRU0
PRU1_FOLDER=/usr/PRU1
C37118_FOLDER=/usr/C37118

echo ""
echo "**********************STARTING D-PMU PROGRAM***************************"

echo "Configuring pins:"
echo ""
######################## Configuring pins for PRU0 #############################
config-pin -a P9_25 pruout 	#LED0
config-pin -q P9_25 		#Use this to verify the pin behaviour
config-pin -a P9_27 pruout 	#LED1
config-pin -q P9_27 		#Use this to verify the pin behaviour
config-pin -a P9_41 gpio 	#just to disconect the virtual pin from P9_24 physical pin
config-pin -q P9_41 		#Use this to verify the pin behaviour
config-pin -a P9_91 pruin  	#Button0 P9_91 = P9_41B - P9_41 reads R31 position 16. To read P9_41, use P9_91 #Chip
config-pin -q P9_91 		#Use this to verify the pin behaviour
config-pin -a P9_17 pwm  	#default- as input PWM sincronization pin (input)
config-pin -q P9_17 		#Use this to verify the pin behaviour
config-pin -a P9_22 pwm 	#(PWM OUTPUT - interconected by cape to P9_28) - if alterated, must alterate P9_28 too 
config-pin -q P9_22 		#Use this to verify the pin behaviour
config-pin -a P9_28 pruin 	#CONVSTAB INPUT for PRU0 counter
config-pin -q P9_28 		#Use this to verify the pin behaviour
config-pin -a P9_11 uart 	#Uart pin to ARM
config-pin -q P9_11 		#Use this to verify the pin behaviour
config-pin -a P9_31 pruout 	#OS2 bit2
config-pin -q P9_31 		#Use this to verify the pin behaviour
config-pin -a P9_29 pruout 	#OS1 bit1
config-pin -q P9_29 		#Use this to verify the pin behaviour
config-pin -a P9_30 pruout 	#OS0 bit0
config-pin -q P9_30 		#Use this to verify the pin behaviour
config-pin -a P9_24 pruin 	#BUSY pin for PRU_X
config-pin -q P9_24 		#Use this to verify the pin behaviour
config-pin -a P9_26 pruin 	#BUSY pin for PRU_Y
config-pin -q P9_26 		#Use this to verify the pin behaviour

######################## Configuring pins for PRU1 #############################
#config-pin -a $HEADER$PIN_NUMBER pruout
#config-pin -q $HEADER$PIN_NUMBER
#to see a list of modes use:> config-pin -l P9_41
config-pin -a P8_39 pruout 	#CS (active low) Chip select
config-pin -q P8_39 		#Use this to verify the pin behaviour
config-pin -a P8_43 pruout 	#READ chip clock control RD/SCL:
config-pin -q P8_43 		#Use this to verify the pin behaviour
config-pin -a P9_26 pruin 	#BUSY signal input
config-pin -q P8_26 		#Use this to verify the pin behaviour
config-pin -a P8_46 pruout 	#RESET_ Pin, not DB4 signal anymore ADC_RESET_pin as output
config-pin -q P8_46 		#Use this to verify the pin behaviour
config-pin -a P8_40 pruin 	#DB7 signal input
config-pin -q P8_40 		#Use this to verify the pin behaviour
config-pin -a P8_45 pruin 	#DB8 signal input
config-pin -q P8_45 		#Use this to verify the pin behaviour
config-pin -a P8_29 pruin 	#S4 button
config-pin -q P8_29 		#Use this to verify the pin behaviour
config-pin -a P8_28 pruout 	#LED2 debug
config-pin -q P8_28 		#Use this to verify the pin behaviour
config-pin -a P8_27 pruout 	#LED3 debug
config-pin -q P8_27 		#Use this to verify the pin behaviour

#****************************************************************************#
# 								Script part for PRU0                         #
#****************************************************************************#
echo "Preparing PRU0 firmware"
echo ""
#Removing old files
	rm $PRU0_FOLDER/loaderPRU0
	rm $PRU0_FOLDER/*.bin

#Compiling loader 
	gcc -o $PRU0_FOLDER/loaderPRU0 $PRU0_FOLDER/loader_PRU0.c -lm -lprussdrv 

#Placing the firmware
	hexpru $PRU0_FOLDER/bin.cmd $PRU0_FOLDER/*.out
	
sleep 1
echo "Removing Stand-by mode from ADC"
echo ""
#Removing the STAND-By (pin P8_33) mode
echo 'out' > /sys/class/gpio/gpio9/direction  #changing to output pin
echo '1' > /sys/class/gpio/gpio9/value  #changing this pin to high level

echo "Configuring voltage range on ADC"
echo ""
#Seting the RANGE  Pin (pin P8_38) to  mode (0 to +-5V, 1 to +-10V  
echo 'out' > /sys/class/gpio/gpio79/direction  #changing to output pin
echo '1' > /sys/class/gpio/gpio79/value  #changing this pin to high level

echo "Placing firmware on PRU0"
echo ""
#Starting PRU
$PRU0_FOLDER/loaderPRU0 text.bin data.bin

echo "Verifying the D-PMU wait or auto run mode pin"
echo ""
###### Verify the execution mode if the Cape have the selection jumper #######
#vdd pin
config-pin -a P8_8 gpio_pu #Allow vdd to the input evaluation pin.
echo 'out' > /sys/class/gpio/gpio67/direction
echo '1' > /sys/class/gpio/gpio67/value	
sleep 1 
 
#evaluation pin
config-pin -a P8_7 gpio_pu #gpio_pd (Pull-down gives a preference to wait mode, pull-up gives preference to auto run)
echo 'in' > /sys/class/gpio/gpio66/direction
sleep 1 

#gnd pin
config-pin -a P8_9 gpio_pd #Allow gnd to the input evaluation pin.
echo 'out' > /sys/class/gpio/gpio69/direction
echo '0' > /sys/class/gpio/gpio69/value	

sleep 1 

runmode_value=$(cat /sys/class/gpio/gpio66/value)
if [ $runmode_value -eq 1 ]; then	#Auto play mode
	echo "Auto run mode enabled, starting the PWM correct operation (Set by PRU0 program). S4 pressed can stop this script."
	echo ""
#****************************************************************************#
# 				Script part for PRU0 - CHECK PWM Start                       #<-IMPORTANT
#****************************************************************************#
	#temporaly transform the Convrequest pin from PRU0 to generic GPIO with pull-up
	config-pin -a P9_28 gpio_pu
	echo 'in' > /sys/class/gpio/gpio113/direction	#same as P9_28
	#temporaly transform the P8_29 pin to reboot abort signal. The user just need to
	#maintein it pressed. 
	config-pin -a P8_29 gpio_pd
	echo 'in' > /sys/class/gpio/gpio87/direction	#same as P8_29 = S4 button
	if [ $(cat /sys/class/gpio/gpio87/value) -eq 0 ]; then #Just verify(AND REBOOT) if button S4 is released
	echo "S4 not pressed."

		#reset OF control variables.
		P9_28_get_low=0 
		P9_28_get_high=0
		pwm_working_fine=0
		for i in {1..600} 
		do
			value_from_convreq_pin=$(cat /sys/class/gpio/gpio113/value)
			if [ $value_from_convreq_pin -eq 1 ]; then
				P9_28_get_high=1	#get a high level over pin
			else
				P9_28_get_low=1		#get a low level over pin
			fi
			if [ $P9_28_get_high -eq $P9_28_get_low ] ; then
				pwm_working_fine=1	#Both signal leves are obtained
				break
			fi
		done

		if [ $pwm_working_fine -eq 0 ]; then
			echo "ERROR: there is no PWM running status. Rebooting"
			reboot now
		fi
	else	
		echo "S4 pressed during plug_and_play script. ABORTING!"
		exit 2 #2	Misuse of shell builtins (according to Bash documentation)	empty_function() {}	Missing keyword or command, or permission problem (and diff return code on a failed binary file comparison).
	fi
	
	echo "D-PMU PWM working fine."
	echo ""
	
	config-pin -a P9_28 pruin 	#Returning the pin mode to PRU0
	config-pin -a P8_29 pruin	#Returning the pin mode to PRU1

	#****************************************************************************#
	# 							  Script part for C37118                         #
	#****************************************************************************#
	echo "Adding default gateway. To get an internet access"
	echo ""
	#Adding the USB-PC (usually as IP 1) as a gateway for beaglebone(when connected directly)
	sudo route add default gw 192.168.7.1 
	
	echo "Executing protocol C program by screen app"
	echo ""
	#Create screen without attaching it
	screen -d -m -S screen_c37118 #screemname = screen_c37118

	#acess the protocol program folder through screen
	screen -S screen_c37118 -X stuff 'cd /usr/c37118' 
	screen -S screen_c37118 -X stuff $(echo -ne '\015')		#enter command

	#Execute program
	#If the internet socket is not initialized. If the gateway is 0.0.0.0 ('sudo rout -n' to verify), try to run "sudo route add default gw 192.168.1.1" Ip that user want
	#screen -S screen_c37118 -X stuff "./c_37118_protocol -s N -i $PDC_IP -p $PDC_PORT"	#BBB connected by USB cable - Old version
	screen -S screen_c37118 -X stuff "./c_37118_protocol"
	screen -S screen_c37118 -X stuff $(echo -ne '\015')		#send enter command (execute C program)
	sleep 1

	#****************************************************************************#
	# 					  Script part for pmu synchrophasor                      #
	#****************************************************************************#
	echo "Executing synchrophasor C program by screen app"
	echo ""
	#Create screen without attaching it
	screen -d -m -S screen_pmu_bbb #screemname = screen_c37118

	##comando para acessar pasta
	screen -S screen_pmu_bbb -X stuff 'cd /usr/synchrophasor'
	screen -S screen_pmu_bbb -X stuff $(echo -ne '\015')		#enter command

	##comando para executar o programa
	screen -S screen_pmu_bbb -X stuff 'sudo ./executar_serial_E_fasor.sh'
	screen -S screen_pmu_bbb -X stuff $(echo -ne '\015')		#enter command
	sleep 1

	#****************************************************************************#
	# 					   Script part for Samples Handler                       #
	#****************************************************************************#
	echo "Executing samples handler C program by screen app"
	echo ""
	#Create screen without attaching it
	screen -d -m -S screen_samples_handler #screemname = screen_c37118

	##comando para acessar pasta
	screen -S screen_samples_handler -X stuff 'cd /usr/sampleshandler'
	screen -S screen_samples_handler -X stuff $(echo -ne '\015')		#enter command

	##comando para executar o programa
	screen -S screen_samples_handler -X stuff './Samples_handler'
	screen -S screen_samples_handler -X stuff $(echo -ne '\015')		#enter command
	sleep 1

	#****************************************************************************#
	# 								Script part for PRU1                         #
	#****************************************************************************#
	echo "Compiling and running PRU1 firmware"
	echo ""
	rm $PRU1_FOLDER/loaderPRU1
	rm $PRU1_FOLDER/*.bin

	#compiling loader
	gcc -o $PRU1_FOLDER/loaderPRU1 $PRU1_FOLDER/loader_PRU1.c -lprussdrv
	#gcc -o $PRU1_FOLDER/loaderPRU1 $PRU1_FOLDER/loader_PRU1_emulate_samples.c -lprussdrv    #Opperate by emulating the samples

	#Placing the firmware
	hexpru $PRU1_FOLDER/bin.cmd $PRU1_FOLDER/*.out
	
	#Starting PRU
	chmod 777 $PRU1_FOLDER/loaderPRU1
	 #nohup creates the file nohup.out (overflow the free memory) - add ">/dev/null 2>&1 &" to dont create nohup.out and execute in background
	#nohup 	nice -n -15	 ./loaderPRU1 text.bin data.bin >/dev/null 2>&1 &

	#Running with printf enable
	nice -n -15	 $PRU1_FOLDER/loaderPRU1 $PRU1_FOLDER/text.bin $PRU1_FOLDER/data.bin &    # Run without print
		#echo "Running with printf enable"
		#nice -n -15	 ./loaderPRU1 text.bin data.bin			#use with print

	echo "D-PMU scripts finished. D-PMU should be send phasors to PDC."
	echo ""	
	
else #Mode is in Wait mode (user should start D-PMU execution)
	echo "D-PMU is in wait mode. User can edit scripts and programs"
	echo "Verify the jumper configuration to change this mode."
fi

echo "############# End: plug_and_play script ############"
